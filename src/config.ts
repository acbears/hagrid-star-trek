
import { StarTrekConfig } from './star-trek/star-trek.config';

export const starTrekConfig = (): StarTrekConfig => ({
    demoField: 'demoFieldOne',
    stage: process.env.STAGE || 'local',
});

export const appConfig = {
    port: process.env.SERVICE_PORT || 8080,
    requestLimit: process.env.REQUEST_LIMIT || '50mb',
    basePath: process.env.SERVICE_BASE_PATH || '/star-trek-service',
    stage: process.env.STAGE,
};

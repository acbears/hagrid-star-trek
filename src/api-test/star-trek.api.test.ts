import { HttpStatus } from '@nestjs/common';
import { config } from 'dotenv';
import * as request from 'supertest';

config({ path: `.env` });
// the url of the BuS API. Defaults to the dev stage when testing locally
const busApiUrl = (process.env.STAGE === 'local') ? `https://api.dev.services.vwfs.io` : `https://api.${process.env.STAGE}.services.vwfs.io`;
const serviceUrl = process.env.STAGE === 'local' ? 'http://localhost:8080/star-trek-service' : `https://api.${process.env.STAGE}.services.vwfs.io/star-trek-service`;

const headers = {
    'content-type': 'application/json',
    accept: 'application/json',
    'x-api-key': process.env.API_KEY || '',
    ...(process.env.STAGE === 'local') ? { 'x-consumer-id': '1234' } : {},
};

describe(`${serviceUrl}/star-trek/:id`, () => {

    test('Get entry', async() => {
        await request(serviceUrl).
            get('/star-trek/bcb32240-b4c2-4e90-822b-37087bdb4839').
            set(headers).
            expect(HttpStatus.OK);
    });
});

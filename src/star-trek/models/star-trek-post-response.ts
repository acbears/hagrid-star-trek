import { Expose, Exclude } from 'class-transformer';
import { IsNotEmpty, IsUUID } from 'class-validator';

@Exclude()
export class StarTrekPostResponse {
    /**
     * Star Trek id
     */
    @Expose()
    @IsNotEmpty()
    @IsUUID()
    public id: string;
}

import { Expose, Exclude } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

@Exclude()
export class StarTrekPostRequest {
    /**
     * a string example
     */
    @Expose()
    @IsNotEmpty()
    @IsString()
    public foo: string;
}

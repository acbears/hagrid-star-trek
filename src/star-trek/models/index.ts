export { StarTrekGetResponse } from './star-trek-get-response';
export { StarTrekPostRequest } from './star-trek-post-request';
export { StarTrekPostResponse } from './star-trek-post-response';
export { StarTrekPathParams } from './star-trek-path-params';

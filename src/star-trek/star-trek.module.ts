import { StarTrekService, ValueGenerator } from './exports';
import { StarTrekController } from './star-trek.controller';
import { starTrekConfig } from './exports';
import { Module } from '@nestjs/common';

@Module({
    providers: [
        StarTrekService,
        ValueGenerator,
        { provide: 'Config', useFactory: starTrekConfig },
    ],
    controllers: [
        StarTrekController,
    ],
})
export class StarTrekModule {
}

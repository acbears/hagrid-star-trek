import { Injectable } from '@nestjs/common';
import { StarTrekPostRequest, StarTrekPostResponse, StarTrekGetResponse } from '../models';

@Injectable()
export class StarTrekService {
    public async create(body: StarTrekPostRequest, id: string): Promise<StarTrekPostResponse> {
        return { id };
    }
    public async retrieve(id: string): Promise<StarTrekGetResponse> {
        return { id };
    }
}

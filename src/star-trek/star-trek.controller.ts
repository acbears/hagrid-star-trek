import { ValueGenerator, StarTrekService } from './exports';
import { ValidationPipe, ApiGatewayMetadata, Metadata, BusApiOperation, ConsumerIdGuard } from '@bus/common';
import { StarTrekPostRequest, StarTrekPostResponse, StarTrekPathParams, StarTrekGetResponse } from './models';
import { Body, Param, Controller, UseGuards, Inject } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Logger } from 'winston';

@Controller('/star-trek')
@UseGuards(ConsumerIdGuard)
@ApiBearerAuth()
@ApiUseTags('Star Trek')
export class StarTrekController {
    constructor(
        private valueGenerator: ValueGenerator,
        private starTrekService: StarTrekService,
        @Inject('winston') private logger: Logger,
    ) {
    }

    /**
     * Creates a star trek.
     */
    @BusApiOperation({
        post: '/',
        title: 'Create Star Trek',
        description: 'Creates a star trek.',
        responseType: StarTrekPostResponse,
        responseDescription: 'Returns the stored star trek and its unique identifiers',
        responseStatus: 201,
    })
    public async createStarTrek(
        @Metadata() metadata: ApiGatewayMetadata,
        @Body(new ValidationPipe(false)) body: StarTrekPostRequest,
    ): Promise<StarTrekPostResponse> {
        this.logger.info('FLOW STEP 1 ValueGenerator.uuid START.', metadata);
        const newId = this.valueGenerator.uuid();
        this.logger.info('FLOW STEP 1 ValueGenerator.uuid COMPLETE.', metadata);
        this.logger.info('FLOW STEP 2 StarTrekService.create START.', metadata);
        return this.starTrekService.create(body, newId);
    }

    /**
     * Get star trek for a star trek id.
     */
    @BusApiOperation({
        get: '/:id',
        title: 'Get Star Trek',
        description: 'Get star trek for a star trek id.',
        responseType: StarTrekGetResponse,
        responseDescription: 'Star Trek.',
    })
    public async getStarTrek(
        @Metadata() metadata: ApiGatewayMetadata,
        @Param(new ValidationPipe(false)) pathParams: StarTrekPathParams,
    ): Promise<StarTrekGetResponse> {
        this.logger.info('FLOW STEP 1 StarTrekService.retrieve START.', metadata);
        return this.starTrekService.retrieve(pathParams.id);
    }
}

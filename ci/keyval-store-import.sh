#!/bin/bash

props=$1
echo "Reading keys in $props"
if [ -f "$props" ]
then
    echo "Reading passed key values"
    while IFS= read -r var
    do
    if [ ! -z "$var" ]
    then
        echo "Adding: $var"
        export "$var"
    fi
    done < "$props"
fi

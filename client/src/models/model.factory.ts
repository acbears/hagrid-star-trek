import { ModelFactory, ModelDescriptions } from '@bus/models';
import * as mergedModel from './merged.lmodels.json';

const starTrekModel = mergedModel as any as ModelDescriptions;
export const StarTrekModelFactory = ModelFactory(starTrekModel);

## Description
This is the BFF (Business Functions Framework) service boilerplate. This can help you to start developing your new service, as it contains the structure and libraries that are being used in BFF. 

## Environment Requirements

Go to [Environment Setup](https://docs.services.vwfs.io/docs/environment_setup/)

## Build & Start

The bus-service-boilerplate starts with default configuration values, e.g. the service port, the service base path, etc. However, you can define during start time environment variables that will override configuration values. These are:

* SERVICE_PORT: default is `8080`
* STAGE: possible values are `local, dev, int, cons, prod`. Default is `local`
* SERVICE_BASE_PATH: default is defined in service.json
* REQUEST_LIMIT: default is `50mb`

This can be set before doing any call or in an environment file .env in the project. After these are set, you are ready to go :-)

### Locally w/ Node

To start the server:
* run `yarn && yarn start` if you have a .env file with all your environmen variables
* run `yarn && ENV_VAR=bar yarn start` if you don't have a .env file

The server will be listening on the default url `localhost:8080`. If you want to change the port, then set the environment variable SERVICE_PORT.

You can check the health of the service in your browser under http://localhost:8080/your_service_name/health

### Using Docker
* Run `docker build -t your_service_docker_image_tag .` to build the docker image
* Run `docker run -i -p 8080:8080 -e STAGE=local -e SERVICE_BASE_PATH=your_service_path your_service_docker_image_tag`. Note that you can specify your *service path* by using the environment variable **SERVICE_BASE_PATH**. 
* Check the health of the service in your browser under http://localhost:8080/your_service_name/health
* Have fun ;-)

## Further Available Scripts

* Run linting: `yarn lint`
* Run unit tests: `yarn test`
* Run API tests: 
    * Create env file named `.env.e2e.<stage>` for api tests in the source project folder and include your environment variables for the test, e.g. STAGE and API_KEY
    * Run `STAGE=local API_KEY=your_api_key yarn test:e2e:stage` 

## Further Development

###Using @busdk Service Clients Stubs

In BFF we have developed client stubs that can be used to invoke our APIs. At the moment, this service boilerplate is **not** using any of them. If you want to use them, include in the *dependencies* of the `package.json` the following packages:

```json
...
    "@busdk/transaction-service": "0.0.12",
    "@busdk/virus-scan-service": "0.0.5",
    "@busdk/document-service": "^0.0.6",
    "@busdk/credit-decision-service": "^0.0.3",
    "@busdk/identification-service": "^0.0.5",
    "@busdk/media-storage-service": "^0.0.7",
    "@busdk/storefront-service": "^0.1.0",
...
```

### Service Development (updating your Node-TS-Project)

For updating your project with the latest versions of the packages, run `yarn api-gen:project`. This will help you to be up to date with the libraries that we update in BFF

### Service Development (adapting your service API)

There are two tasks that you must perform before writing any line of business logic code:
1. if you need to adapt the request and response models, then adapt the *.models.json model under src/*/models. This are essentially the data models that you will use in your API. 

2. if you need to adapt your API definition, i.e. your endpoints, then adapt the *.api.json under src/

3. To generate a new swagger, run `yarn api-gen:models`

4. To generate your new API skeleton, run `api-gen:service`

5. Your business logic can now be developed under injectables for the ones that you defined in *.api.json


### Tests Development
At the moment, we included a boilerplate for unit and API tests:
* Unit tests are located in src/*/ and have the file name format `your_service_name.service.spec.ts`.
* API tests are located under src/*/api and have the file name format `<service_name>.e2e.test.ts`. 

Unit and API tests are developed by you

## Technical Diagram

Here the technical architectural overview diagram of your service should come. 

## Contact
Business Services @BusinessFunctions <business-service-team@digitalunit.berlin> or #business-services-support channel @Rocketchat
